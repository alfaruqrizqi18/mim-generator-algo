class MemesModel {
  bool success;
  Data data;

  MemesModel({
    this.success = false,
    required this.data,
  });

  static MemesModel fromJson(Map<String, dynamic> json) {
    return MemesModel(
      success: json['success'] ?? false,
      data: json['data'] != null ? Data.fromJson(json['data']) : Data(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['success'] = success;
    data['data'] = data;
    return data;
  }
}

class Data {
  List<Memes> memes;

  Data({this.memes = const []});

  static Data fromJson(Map<String, dynamic> json) {
    List<dynamic> memesList = json['memes'];
    return Data(
        memes: memesList.isEmpty
            ? []
            : List<Memes>.from(
                memesList.map((i) => Memes.fromJson(i)),
              ));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['memes'] = memes.toList();
    return data;
  }
}

class Memes {
  String id;
  String name;
  String url;
  num width;
  num height;
  num boxCount;
  num captions;

  Memes({
    this.id = "",
    this.name = "",
    this.url = "",
    this.width = 0,
    this.height = 0,
    this.boxCount = 0,
    this.captions = 0,
  });

  static Memes fromJson(Map<String, dynamic> json) {
    return Memes(
      id: json['id'] ?? "",
      name: json['name'] ?? "",
      url: json['url'] ?? "",
      width: json['width'] ?? 0,
      height: json['height'] ?? 0,
      boxCount: json['box_count'] ?? 0,
      captions: json['captions'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['url'] = url;
    data['width'] = width;
    data['height'] = height;
    data['box_count'] = boxCount;
    data['captions'] = captions;
    return data;
  }
}
