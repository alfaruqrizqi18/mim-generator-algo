import 'package:dio/dio.dart';

Future<Response<dynamic>> getRequest({
  required String path,
  Map<String, dynamic> newOptions = const {},
  Map<String, dynamic> queryParamsAsMap = const {},
}) async {
  Dio dio = Dio();

  Uri url = Uri.parse(path);

  final response = await dio.get(
    url.toString(),
    queryParameters: queryParamsAsMap,
  );

  return response;
}
