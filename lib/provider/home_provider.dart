import 'package:flutter/foundation.dart';
import 'package:meme_generator_algo_test/models/memes_model.dart';
import 'package:meme_generator_algo_test/utils/api.dart';

class HomeProvider extends ChangeNotifier {
  HomeState state = HomeState(
    memesModel: MemesModel(
      data: Data(),
    ),
  );

  String memesEndpointUrl = "https://api.imgflip.com/get_memes";

  Future<void> getMemes() async {
    state.isLoading = true;
    try {
      var response = await getRequest(path: memesEndpointUrl);
      if (response.statusCode == 200) {
        state.memesModel = MemesModel.fromJson(response.data);
        resetError();
        notifyListeners();
      } else {
        state.isError = true;
      }
    } catch (e) {
      state.isError = true;
    }
    state.isLoading = false;
    notifyListeners();
  }

  resetError() {
    state.isError = false;
  }
}

class HomeState {
  bool isLoading;
  bool isError;
  MemesModel memesModel;
  HomeState({
    this.isLoading = true,
    this.isError = false,
    required this.memesModel,
  });
}
