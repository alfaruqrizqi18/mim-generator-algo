// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:meme_generator_algo_test/screens/detail_page.dart';
import 'package:provider/provider.dart';

import '../provider/home_provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPersistentFrameCallback((timeStamp) {
      getMemes();
    });
  }

  getMemes() {
    Provider.of<HomeProvider>(context, listen: false).getMemes();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Mim Generator"),
            centerTitle: true,
          ),
          body: _getWidgetBasedOnState(provider.state),
        );
      },
    );
  }

  Widget _getWidgetBasedOnState(HomeState state) {
    if (state.isLoading) {
      return Center(
        child: Text("Sedang memuat..."),
      );
    }

    if (state.isError) {
      return GestureDetector(
        onTap: () {
          getMemes();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Center(
            child: Text(
              "Memuat data gagal. Ketuk tulisan ini untuk memuat ulang",
              textAlign: TextAlign.center,
            ),
          ),
        ),
      );
    }

    return _successContent(state);
  }

  Widget _successContent(HomeState state) {
    return RefreshIndicator(
      child: GridView.builder(
          itemCount: state.memesModel.data.memes.length,
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          itemBuilder: (ctx, index) {
            var item = state.memesModel.data.memes[index];
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) {
                      return DetailPage(memes: item);
                    },
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1)),
                child: Image.network(
                  item.url,
                  fit: BoxFit.contain,
                ),
              ),
            );
          }),
      onRefresh: () async {
        getMemes();
      },
    );
  }
}
