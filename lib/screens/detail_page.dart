// ignore_for_file: prefer_const_constructors, sort_child_properties_last

import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:meme_generator_algo_test/models/memes_model.dart';
import 'package:meme_generator_algo_test/provider/home_provider.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({super.key, required this.memes});
  final Memes memes;

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  File? imageGallery;
  TextEditingController textController = TextEditingController();
  String text = "";
  ScreenshotController screenshotController = ScreenshotController();

  double width = 100.0, height = 100.0;
  Offset? position;

  @override
  void initState() {
    super.initState();
    position = Offset(0.0, height - 20);
  }

  pickImage() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      setState(() {
        imageGallery = File(image.path);
      });
    }
  }

  saveImageToLocal() async {
    await screenshotController
        .capture(delay: const Duration(milliseconds: 10))
        .then((Uint8List? image) async {
      if (image != null) {
        ImageGallerySaver.saveImage(Uint8List.fromList(image),
            quality: 60, name: "hello");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Gambar sudah disimpan"),
            behavior: SnackBarBehavior.floating,
          ),
        );
      }
    });
  }

  shareTo() async {
    await screenshotController
        .capture(delay: const Duration(milliseconds: 10))
        .then((Uint8List? image) async {
      if (image != null) {
        final directory = await getApplicationDocumentsDirectory();
        final imagePath = await File('${directory.path}/image.png').create();
        await imagePath.writeAsBytes(image);
        await Share.shareFiles([imagePath.path]);
      }
    });
  }

  showTextField() {
    showModalBottomSheet(
      context: context,
      isDismissible: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.background,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
            ),
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                TextField(
                  controller: textController,
                  decoration: InputDecoration(
                      hintText: "Text....", border: InputBorder.none),
                ),
                ElevatedButton.icon(
                  onPressed: () {
                    setState(() {
                      text = textController.text;
                    });
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.check),
                  label: Text("Simpan"),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          backgroundColor: Theme.of(context).colorScheme.background,
          appBar: AppBar(
            title: Text("Mim Generator"),
            centerTitle: true,
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: TextButton(
                  onPressed: () {
                    saveImageToLocal();
                  },
                  child: Text("Simpan"),
                ),
              )
            ],
          ),
          bottomNavigationBar: buildBottomBar(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              shareTo();
            },
            child: Icon(Icons.share),
            elevation: 0.9,
          ),
          body: ListView(
            children: [
              buildInfoCard(),
              Screenshot(
                controller: screenshotController,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(border: Border.all(width: 1)),
                      child: Image.network(
                        widget.memes.url,
                      ),
                    ),
                    if (imageGallery != null) ...[
                      Container(
                        child: Image.file(
                          imageGallery!,
                          height: 150,
                        ),
                        alignment: Alignment.center,
                      )
                    ],
                    if (text != "") ...[
                      Positioned(
                        child: Draggable(
                          child: Container(
                            width: width,
                            height: height,
                            color: Theme.of(context).colorScheme.surface,
                            alignment: Alignment.center,
                            child: Text(
                              text,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 20,
                                  color:
                                      Theme.of(context).colorScheme.onSurface),
                            ),
                          ),
                          feedback: Material(
                            child: Container(
                              width: width,
                              height: height,
                              color: Theme.of(context).colorScheme.surface,
                              child: Text(
                                text,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onSurface),
                              ),
                            ),
                          ),
                          onDraggableCanceled:
                              (Velocity velocity, Offset offset) {
                            setState(() {
                              position = offset;
                            });
                          },
                        ),
                        left: position?.dx,
                        top: position?.dy,
                      )
                    ],
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget buildInfoCard() {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
      child: ListTile(
        isThreeLine: true,
        dense: true,
        leading: CircleAvatar(
          backgroundColor: Theme.of(context).colorScheme.primaryContainer,
          child: Icon(
            Icons.info,
            color: Theme.of(context).colorScheme.onPrimaryContainer,
          ),
        ),
        title: Text("Informasi"),
        subtitle: Text(
          "Untuk text, kamu bisa menggesernya. Tapi sedikit hati-hati ya, positioningnya agak buggy. Apabila text tidak sengaja hilang, kamu bisa kembali ke halaman awal kemudian klik salah satu gambar",
        ),
      ),
    );
  }

  Widget buildBottomBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            width: 0.5,
            color: Theme.of(context).colorScheme.outline,
          ),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: ListTile(
                  onTap: () {
                    pickImage();
                  },
                  title: Text("Add logo"),
                  leading: CircleAvatar(
                    backgroundColor: Theme.of(context).colorScheme.surface,
                    child: Icon(
                      Icons.image,
                      color: Theme.of(context).colorScheme.onSurface,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ListTile(
                  onTap: () {
                    showTextField();
                  },
                  title: Text("Add text"),
                  leading: CircleAvatar(
                    backgroundColor: Theme.of(context).colorScheme.surface,
                    child: Icon(
                      Icons.text_fields_rounded,
                      color: Theme.of(context).colorScheme.onSurface,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
